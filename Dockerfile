FROM python:3.13.1-alpine

# Speedtest CLI Version
ARG SPEEDTEST_VERSION=1.2.0

# Create user
RUN adduser -D speedtest

WORKDIR /app

COPY src/. .

# Install required modules and Speedtest CLI
RUN python3 -m pip install --no-cache-dir -r requirements.txt && \
    wget -nv -O /tmp/speedtest.tgz "https://install.speedtest.net/app/cli/ookla-speedtest-${SPEEDTEST_VERSION}-linux-x86_64.tgz" && \
    tar zxvf /tmp/speedtest.tgz -C /tmp && \
    cp /tmp/speedtest /usr/local/bin && \
    chown -R speedtest:speedtest /app && \
    rm -rf \
     /tmp/*

USER speedtest

CMD ["python", "-u", "exporter.py"]

