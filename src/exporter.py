import subprocess
import json
from os import getenv
import logging
import datetime
from prometheus_client import make_wsgi_app, Gauge, Info
from flask import Flask
from waitress import serve
from shutil import which

app = Flask("Speedtest-Exporter")  # Create flask app

# Setup logging values
format_string = 'level=%(levelname)s datetime=%(asctime)s %(message)s'
logging.basicConfig(encoding='utf-8',
                    level=logging.DEBUG,
                    format=format_string)

# Disable Waitress Logs
log = logging.getLogger('waitress')
log.disabled = True

# Create Metrics
server = Gauge('speedtest_server_id', 'Speedtest server ID used to test')
jitter = Gauge('speedtest_jitter_latency_milliseconds', 'Speedtest current Jitter in ms')
ping = Gauge('speedtest_ping_latency_milliseconds', 'Speedtest current Ping in ms')
download_speed = Gauge('speedtest_download_bits_per_second', 'Speedtest current Download Speed in bit/s')
upload_speed = Gauge('speedtest_upload_bits_per_second', 'Speedtest current Upload speed in bits/s')
up = Gauge('speedtest_up', 'Speedtest status whether the scrape worked')
server_name_info = Info('speedtest_server_name', 'Speedtest server name used to test')
server_hostname = Info('speedtest_server_host', 'Speedtest server host used to test')
latency_max_seconds = Gauge('speedtest_ping_max_latency_milliseconds', 'Speedtest max Ping in ms')
latency_min_seconds = Gauge('speedtest_ping_min_latency_milliseconds', 'Speedtest min Ping in ms')
download_latency_avg_seconds = Gauge('speedtest_download_avg_latency_milliseconds', 'Speedtest avg Download in ms')
download_latency_max_seconds = Gauge('speedtest_download_max_latency_milliseconds', 'Speedtest max Download Ping in ms')
download_latency_min_seconds = Gauge('speedtest_download_min_latency_milliseconds', 'Speedtest min Download Ping in ms')
download_latency_jitter_seconds = Gauge('speedtest_download_jitter_milliseconds', 'Speedtest download Jitter in ms')
upload_latency_avg_seconds = Gauge('speedtest_upload_avg_latency_milliseconds', 'Speedtest avg Upload in ms')
upload_latency_max_seconds = Gauge('speedtest_upload_max_latency_milliseconds', 'Speedtest max Upload in ms')
upload_latency_min_seconds = Gauge('speedtest_upload_min_latency_milliseconds', 'Speedtest min Upload in ms')
upload_latency_jitter_seconds = Gauge('speedtest_upload_jitter_latency_milliseconds', 'Speedtest Upload Jitter in ms')
packet_loss_ratio = Gauge('speedtest_packet_loss_ratio', 'Ratio of Packet Loss')
timestamp_info = Gauge('timestamp', 'Timestamp')
nodename_info = Info('nodename_info', 'Node Name')
external_ip_info = Info('external_ip','External IP')

# Cache metrics for how long (seconds)?
cache_seconds = int(getenv('SPEEDTEST_CACHE_FOR', 0))
cache_until = datetime.datetime.fromtimestamp(0)

def bytes_to_bits(bytes_per_sec):
    return bytes_per_sec * 8


def bits_to_megabits(bits_per_sec):
    megabits = round(bits_per_sec * (10**-6), 2)
    return str(megabits) + "Mbps"


def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError:
        return False
    return True


def runTest():
    serverID = getenv('SPEEDTEST_SERVER')
    timeout = int(getenv('SPEEDTEST_TIMEOUT', 90))
    nodeID = getenv('NODENAME')

    cmd = [
        "speedtest", "--format=json-pretty", "--progress=no",
        "--accept-license", "--accept-gdpr"
    ]
    if serverID:
        cmd.append(f"--server-id={serverID}")
    try:
        output = subprocess.check_output(cmd, timeout=timeout)
    except subprocess.CalledProcessError as e:
        output = e.output
        if not is_json(output):
            if len(output) > 0:
                logging.error('Speedtest CLI Error occurred that' +
                              'was not in JSON format')
            return (0, 0, 0, 0, 0, 0)
    except subprocess.TimeoutExpired:
        logging.error('Speedtest CLI process took too long to complete ' +
                      'and was killed.')
        return (0, 0, 0, 0, 0, 0)

    if is_json(output):
        data = json.loads(output)
        if "error" in data:
            # Socket error
            print('Something went wrong')
            print(data['error'])
            return (0, 0, 0, 0, 0, 0, 0 ,0, 0 ,0, 0 ,0, 0 ,0, 0 ,0, 0 ,0, 0 ,0, 0 ,0)  # Return all data as 0
        if "type" in data:
            if data['type'] == 'log':
                print(str(data["timestamp"]) + " - " + str(data["message"]))
            if data['type'] == 'result':
                actual_server = int(data['server']['id'])
                server_name = data['server']['name']
                server_host = data['server']['host']
                actual_jitter = data['ping']['jitter']
                actual_ping = data['ping']['latency']
                latency_max = data['ping']['high']
                latency_min = data['ping']['low']
                download = bytes_to_bits(data['download']['bandwidth'])
                download_latency_avg = data['download']['latency']['iqm']
                download_latency_max = data['download']['latency']['high']
                download_latency_min = data['download']['latency']['low']
                download_latency_jitter = data['download']['latency']['jitter']
                upload = bytes_to_bits(data['upload']['bandwidth'])
                upload_latency_avg = data['upload']['latency']['iqm']
                upload_latency_max = data['upload']['latency']['high']
                upload_latency_min = data['upload']['latency']['low']
                upload_latency_jitter = data['upload']['latency']['jitter']
                packet_loss = data['packetLoss']
                timestamp = data['timestamp']
                nodename = nodeID
                external_ip = data['interface']['externalIp']
                return (actual_server, server_name, server_host, actual_jitter, actual_ping, latency_max, latency_min, download,
                    download_latency_avg, download_latency_max, download_latency_min, download_latency_jitter,
                    upload, upload_latency_avg, upload_latency_max, upload_latency_min, upload_latency_jitter, packet_loss, timestamp, nodename, external_ip,  1)


@app.route("/metrics")
def updateResults():
    global cache_until

    if datetime.datetime.now() > cache_until:
        r_server, r_server_name, r_server_host, r_jitter, r_ping, r_latency_max, r_latency_min, r_download, r_download_latency_avg, r_download_latency_max, r_download_latency_min, r_download_latency_jitter, r_upload, r_upload_latency_avg, r_upload_latency_max, r_upload_latency_min, r_upload_latency_jitter, r_packet_loss, r_timestamp, r_nodename, r_external_ip, r_status = runTest()
        server.set(r_server)
        jitter.set(r_jitter)
        ping.set(r_ping)
        download_speed.set(r_download)
        upload_speed.set(r_upload)
        up.set(r_status)
        latency_max_seconds.set(r_latency_max)
        latency_min_seconds.set(r_latency_min)
        download_latency_avg_seconds.set(r_download_latency_avg)
        download_latency_max_seconds.set(r_download_latency_max)
        download_latency_min_seconds.set(r_download_latency_min)
        download_latency_jitter_seconds.set(r_download_latency_jitter)
        upload_latency_avg_seconds.set(r_upload_latency_avg)
        upload_latency_max_seconds.set(r_upload_latency_max)
        upload_latency_min_seconds.set(r_upload_latency_min)
        upload_latency_jitter_seconds.set(r_upload_latency_jitter)
        packet_loss_ratio.set(r_packet_loss)
        nodename_info.info({'node_name': {r_nodename}})
        external_ip_info.info({'external_ip': {r_external_ip}})
        timestamp_info.set(r_timestamp)
        logging.info("Server=" + str(r_server) + " Jitter=" + str(r_jitter) +
                     "ms" + " Ping=" + str(r_ping) + "ms" + " Download=" +
                     bits_to_megabits(r_download) + " Upload=" +
                     bits_to_megabits(r_upload))

        cache_until = datetime.datetime.now() + datetime.timedelta(
            seconds=cache_seconds)

    return make_wsgi_app()


@app.route("/")
def mainPage():
    return ("<h1>Welcome to Speedtest-Exporter.</h1>" +
            "Click <a href='/metrics'>here</a> to see metrics.")


def checkForBinary():
    if which("speedtest") is None:
        logging.error("Speedtest CLI binary not found. Please install it by" +
                      " going to the official website.\n" +
                      "https://www.speedtest.net/apps/cli")
        exit(1)
    speedtestVersionDialog = (subprocess.run(['speedtest', '--version'],
                              capture_output=True, text=True))
    if "Speedtest by Ookla" not in speedtestVersionDialog.stdout:
        logging.error("Speedtest CLI that is installed is not the official" +
                      " one. Please install it by going to the official" +
                      " website.\nhttps://www.speedtest.net/apps/cli")
        exit(1)


if __name__ == '__main__':
    checkForBinary()
    PORT = getenv('SPEEDTEST_PORT', 9798)
    logging.info("Starting Speedtest-Exporter on http://localhost:" +
                 str(PORT))
    serve(app, host='0.0.0.0', port=PORT)
